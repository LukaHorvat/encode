module Main where

import System.Environment
import Data.Text.ICU.Convert
import qualified Data.ByteString as BS

encode :: String -> String -> FilePath -> FilePath -> IO ()
encode fromEnc toEnc fromFile toFile = do
    fromFileBs <- BS.readFile fromFile
    fromCvt <- open fromEnc Nothing
    toCvt <- open toEnc Nothing
    let toFileBs = fromUnicode toCvt (toUnicode fromCvt fromFileBs)
    BS.writeFile toFile toFileBs

main :: IO ()
main = do
    args <- getArgs
    case args of
        [fromEnc, toEnc, fromFile, toFile] -> encode fromEnc toEnc fromFile toFile
        _ -> putStrLn "Usage:\n\tencode FROM_ENC TO_ENC FROM_FILE TO_FILE"
